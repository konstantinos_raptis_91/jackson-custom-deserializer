package com.raptis.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.List;

/**
 *
 * @author konst
 */
public class Person {
    
    List<Address> addressList;

    public Person() {
    }

    public Person(List<Address> addressList) {
        this.addressList = addressList;
    }

    @JsonProperty("ADDRESS")
    public List<Address> getAddressList() {
        return addressList;
    }

    @JsonDeserialize(
            as = List.class, 
            contentAs = Address.class, 
            contentUsing = AddressDeserializer.class
    )
    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    @Override
    public String toString() {
        return "Person{" + "Addresses=" + addressList + '}';
    }
    
}
