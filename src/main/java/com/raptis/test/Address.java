/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.raptis.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *
 * @author konst
 */
@JsonPropertyOrder(
        {
            "street",
            "number"
        })
public class Address {
    
    private String street;
    private int number;

    public Address() {

    }

    public Address(String street, int number) {
        this.street = street;
        this.number = number;
    }

    @JsonProperty("STREET")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @JsonProperty("NUMBER")
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Address{" + "Street=" + street + ", Number=" + number + '}';
    }

}
