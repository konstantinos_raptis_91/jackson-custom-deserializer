package com.raptis.test;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

/**
 *
 * @author konst
 */
public class AddressDeserializer extends JsonDeserializer<Address> {

    @Override
    public Address deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(p);
        
        String streetVal = root.path("STREET").asText(null);
        int numberVal = root.path("NUMBER").asInt(-1);

        if (streetVal == null && numberVal == -1) {
            return null;
        }

        ObjectMapper addressMapper = new ObjectMapper();
        Address address = addressMapper.treeToValue(root, Address.class);

        return address;
    }

}
