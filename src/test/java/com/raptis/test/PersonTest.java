package com.raptis.test;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

/**
 *
 * @author konst
 */
public class PersonTest {

    @Test
    public void testPerson() throws Exception {

        String json = "{\n"
                + "    \"ADDRESS\": [\n"
                + "        {},\n"
                + "        {\n"
                + "            \"STREET\": \"Nestoros\",\n"
                + "            \"NUMBER\": 17\n"
                + "        }\n"
                + "    ]\n"
                + "}";

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        mapper.setSerializationInclusion(Include.NON_EMPTY);
        Person p = mapper.readValue(json, Person.class);

        System.out.println(p);
    }

}
